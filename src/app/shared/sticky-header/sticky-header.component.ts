import {AfterViewInit, Component, HostBinding, OnDestroy} from '@angular/core';
import {fromEvent, Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, pairwise, share, takeUntil, throttleTime} from 'rxjs/operators';
import {animate, state, style, transition, trigger} from '@angular/animations';

enum VisibilityState {
  Visible = 'visible',
  Hidden = 'hidden'
}

enum Direction {
  Up = 'Up',
  Down = 'Down'
}

@Component({
  selector: 'app-sticky-header',
  template: '<ng-content></ng-content>',
  styles: [`:host {
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1;
    background-color: #fff;
  }
  `],
  animations: [
    trigger('toggle', [
      state(
        VisibilityState.Hidden,
        style({opacity: 0, transform: 'translateY(-100%)'})
      ),
      state(
        VisibilityState.Visible,
        style({opacity: 1, transform: 'translateY(0)'})
      ),
      transition('* => *', animate('200ms ease-in'))
    ])
  ]
})
export class StickyHeaderComponent implements AfterViewInit, OnDestroy {
  private _isVisible = true;
  private _destroyed$ = new Subject();

  @HostBinding('@toggle')
  get toggle(): VisibilityState {
    return this._isVisible ? VisibilityState.Visible : VisibilityState.Hidden;
  }

  ngAfterViewInit() {
    const scroll$ = fromEvent(window, 'scroll').pipe(
      takeUntil(this._destroyed$),
      throttleTime(10),
      map(() => window.pageYOffset),
      filter(y => y > 68),
      pairwise(),
      map(([y1, y2]): Direction => (y2 < y1 ? Direction.Up : Direction.Down)),
      distinctUntilChanged(),
      share()
    );

    const goingUp$ = scroll$.pipe(
      filter(direction => direction === Direction.Up)
    );

    const goingDown$ = scroll$.pipe(
      filter(direction => direction === Direction.Down)
    );

    goingUp$.subscribe(() => {
      this._isVisible = true;
    });
    goingDown$.subscribe(() => {
      this._isVisible = false;
    });
  }

  ngOnDestroy(): void {
    this._destroyed$.next();
  }

}
