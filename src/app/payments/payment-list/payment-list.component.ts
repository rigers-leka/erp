import {Component, OnInit} from '@angular/core';
import {Payment} from '../../payment';
import {Observable} from 'rxjs';
import {PaymentService} from '../payment.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ConfirmDialogComponent} from '../../shared/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.scss']
})
export class PaymentListComponent implements OnInit {
  public payments: Observable<Payment[]>;

  constructor(
    private paymentService: PaymentService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.payments = this.paymentService.get();
  }

  clickedEdit(payment: Payment) {
  }

  clickedDelete(key: string) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: {title: 'Discard payment?'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.paymentService.delete(key).then(() => {
          this.snackBar.open(`Fatura ${key} u fshi!`, 'Ok');
        });
      }
    });
  }

}
