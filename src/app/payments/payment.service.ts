import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {map} from 'rxjs/operators';
import {Payment} from '../payment';
import {Observable} from 'rxjs';

const paymentPath = 'fatura';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private db: AngularFireDatabase) {
  }

  get(): Observable<Payment[]> {
    return this.db.list<Payment>(paymentPath).snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({key: c.payload.key, ...c.payload.val()}))
        )
      );
  }

  delete(key: string): Promise<void> {
    if (typeof key === 'undefined') {
      console.warn('[PaymentService::delete] Params missing');
      return;
      // throw new Error('Params missing');
    }
    return this.db.list(paymentPath).remove(key);
  }

  create(newPayment: Payment) {
    return this.db.list(paymentPath).push(newPayment);
  }

}
