import {Component, OnInit} from '@angular/core';
import {Payment} from '../../payment';
import {PaymentService} from '../payment.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-payment-form',
  templateUrl: './payment-form.component.html',
  styleUrls: ['./payment-form.component.scss']
})
export class PaymentFormComponent implements OnInit {

  availableStatuses: string[];
  availableValutes: string[];

  newPayment = new Payment();

  constructor(private paymentService: PaymentService, private snackBar: MatSnackBar) {
    this.availableStatuses = ['paid', 'cancelled', 'Scheduled'];
    this.availableValutes = ['Al', 'USD', 'EURO'];
  }

  ngOnInit() {
  }

  ruaj(event) {
    event.preventDefault();

    this.paymentService.create(this.newPayment)
      .then(() => {

        this.snackBar.open('Fatura u ruajt me sukses', 'Ok');

        this.newPayment = {
          title: null,
          description: null,
          status: null,
          total: null,
          valute: null,
        };
      });
  }
}
