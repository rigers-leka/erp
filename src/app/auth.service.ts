import {Injectable} from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import * as firebase from 'firebase/app';

interface User {
  uid: string;
  email?: string | null;
  photoURL?: string;
  displayName?: string;
}

export class EmailPasswordCredentials {
  email: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(public afAuth: AngularFireAuth) {

  }

  // emailSignUp(credentials: EmailPasswordCredentials): firebase.Promise<FirebaseAuthState> {
  //   return this.af.auth.createUser(credentials)
  //     .then(() => console.log("success"))
  //     .catch(error => console.log(error));
  // }
  //
  // emailLogin(credentials: EmailPasswordCredentials): firebase.Promise<FirebaseAuthState> {
  //   return this.af.auth.login(credentials,
  //     {
  //       provider: AuthProviders.Password,
  //       method: AuthMethods.Password
  //     })
  //     .then(() => console.log("success"))
  //     .catch(error => console.log(error));
  // }


  //...omitted
  // resetPassword(email: string) {
  //   var auth = firebase.auth();
  //
  //   return auth.sendPasswordResetEmail(email)
  //     .then(() => console.log("email sent"))
  //     .catch((error) => console.log(error))
  // }

  doGoogleLogin(){
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.afAuth.auth
        .signInWithPopup(provider)
        .then(res => {
          resolve(res);
        })
    })
  }

  doFacebookLogin(){
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.FacebookAuthProvider();
      this.afAuth.auth
        .signInWithPopup(provider)
        .then(res => {
          resolve(res);
        }, err => {
          console.log(err);
          reject(err);
        })
    })
  }
}
