export class Payment {
  key?: string;
  title: string;
  description: string;
  status: string;
  total: number;
  valute: string;
}
