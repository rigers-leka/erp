import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabase} from '@angular/fire/database';

import {environment} from '../environments/environment';

import {DashboardComponent} from './dashboard/dashboard.component';
import {PaymentsViewComponent} from './payments/payments-view/payments-view.component';
import {PaymentFormComponent} from './payments/payment-form/payment-form.component';
import {PaymentListComponent} from './payments/payment-list/payment-list.component';

import {SharedModule} from './shared/shared.module';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {LoginComponent} from './login/login.component';
import {AngularFirestoreModule} from "@angular/fire/firestore";

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    PaymentsViewComponent,
    PaymentFormComponent,
    PaymentListComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,

    // app
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  providers: [
    AngularFireDatabase,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
